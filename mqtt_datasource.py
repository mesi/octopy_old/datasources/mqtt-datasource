#!/usr/bin/python3
import sys
import time
import json
import logging
from os import path
import argparse
import traceback
from lib.octopyapp.octopyapp import OctopyApp
from mqtt_link import MQTTLink

APP_ID = 'MQTT Datasource'
LOG_LEVEL = logging.INFO

UPDATE_INTERVAL = 2

DESCRIBE_SUFFIX = 'description'

class MQTTDatasourceManager(OctopyApp):
    def __init__(self, config):
        super().__init__(APP_ID, LOG_LEVEL, config)
        self._sources = {}
        # Subscribe to API topics
        self.subscribe(self.topics['datasource']['set channels'], self.on_mqtt_set_link_message)
        self.subscribe(self.topics['datasource']['remove channels'], self.on_mqtt_remove_link_message)
        self.subscribe(self.topics['datasource']['config mqtt'], self.on_mqtt_config_datasource_message)
        self.subscribe(self.topics['datasource']['remove source'], self.on_mqtt_remove_datasource_message)


    def start(self):
        super().start(start_loop = False)
        # Wait for MQTT connection
        logging.info("Waiting for MQTT connection")
        while not self.connected:
            self.mqtt_client.loop(0.1)
            self.mqtt_client.loop_misc()
        tick = 0.1  # 10 Hz refresh rate
        last_update = time.time() - UPDATE_INTERVAL
        last_mqtt_misc = time.time()
        # MQTT main loop
        while True:
            try:
                self.mqtt_client.loop(tick)
                current_time = time.time()
                # Check if any datasource has been changed
                dirty = False
                for source in self._sources.values():
                    if source.dirty:
                        dirty = True
                        break
                if dirty or current_time > last_update + UPDATE_INTERVAL:
                    # Publish datasource status and links regularly
                    last_update = current_time
                    self.publish_status()
                if current_time > last_mqtt_misc + 2:
                    # Let the MQTT library do its housekeeping regularly
                    last_mqtt_misc = current_time
                    self.mqtt_client.loop_misc()
            except RuntimeError:
                self.halt('Runtime error')
            except KeyboardInterrupt:
                self.halt('Keyboard interrupt')
            except BaseException as err:
                logging.error(err)
                if LOG_LEVEL == logging.DEBUG:
                    traceback.print_exc()


    def halt(self, reason):
        logging.debug('Exiting: ' + reason)
        for source_id in self._sources.copy().keys():
            self.remove_datasource(source_id)
        self.publish_status()
        self.mqtt_client.loop()
        self.stop()
        sys.exit(0)


    def on_mqtt_config_datasource_message(self, client, userdata, msg):
        logging.debug(f"Config datasource message: {msg.payload}")
        try:
            payload = json.loads(msg.payload)
        except json.decoder.JSONDecodeError as err:
            logging.error(f"JSON encoding error for datasource config message: {err}")
            return
        try:
            payload['describe suffix'] = DESCRIBE_SUFFIX
            source = MQTTDatasource(payload)
        except TypeError as err:
            logging.error(f"Error configuring MQTT datasource: {err}")
            return
        except Exception as err:
            logging.error(f"Could not configure MQTT datasource: {err}")
            return
        if source.id in self._sources:
            logging.info(f"MQTT datasource already exists: '{source.id}'")
            return
        self._sources[source.id] = source
        self.subscribe(source.prefix, source.on_link_message)
        self.publish_status()


    def on_mqtt_remove_datasource_message(self, client, userdata, msg):
        logging.debug(f"Remove datasource message: {msg.payload}")
        try:
            payload = json.loads(msg.payload)
        except json.decoder.JSONDecodeError as err:
            logging.error(f"JSON encoding error for datasource config message: {err}")
            return
        if not isinstance(payload, dict) or not payload.get('datasource id'):
            logging.error(f"Can't delete datasource: missing datasource id or malformed message: {msg.payload}")
            return
        source_id = payload['datasource id']
        self.remove_datasource(source_id)


    def on_mqtt_set_link_message(self, client, userdata, msg):
        logging.debug(f"Set link message: {msg.payload}")
        logging.info(f"Set link is not supported on MQTT datasource")
        pass


    def on_mqtt_remove_link_message(self, client, userdata, msg):
        logging.debug(f"Remove link message: {msg.payload}")
        logging.info(f"Remove link is not supported on MQTT datasource")
        pass


    def publish_status(self, empty = False):
        for source in self._sources.values():
            source.mark_clean()
            topic = path.join(self.topics['datasource']['status'], source.id)
            if empty:
                logging.debug(f"Datasource removed - removing status for datasource '{source.id}'")
                self.publish(topic, None)
            else:
                source_conf = source.serialize()
                payload = json.dumps(source_conf, indent = 4)
                logging.debug(f"Publishing status for datasource '{source.id}'")
                self.publish(topic, payload)


    def remove_datasource(self, source_id):
        if not source_id in self._sources:
            #logging.warning(f"Can't remove datasource. The datasource id '{source_id}' is invalid or non-existent")
            # This comand was probably for another datasource
            return
        try:
            source = self._sources[source_id]
            self.unsubscribe(source.prefix)
            link_topic = path.join(self.topics['datasource']['links'], source.id)
            self.publish(link_topic, None)
            status_topic = path.join(self.topics['datasource']['status'], source.id)
            del self._sources[source.id]
            self.publish(status_topic, None)
        except Exception as err:
            logging.error(f"Error when removing datasource: {err}")



class MQTTDatasource:
    def __init__(self, conf):
        if not isinstance(conf, dict):
            raise TypeError("Invalid type for config. Expected dict. Got {conf.__class__.__name__}")
        prefix = conf.get('prefix')
        if not prefix:
            raise ValueError("Prefix is missing or is empty")
        if prefix.endswith('/#'):
            prefix = prefix[:-2]
        if '#' in prefix:
            raise ValueError("Prefix may only contain '#' wildcard on the end. Got '{prefix}'.")
        self._id = prefix
        self._prefix = path.join(prefix, '#')
        self._links = {}
        self._dirty = True
        self._describe_suffix = conf.get('describe suffix')


    @property
    def id(self):
        return self._id


    @property
    def prefix(self):
        return self._prefix


    @property
    def dirty(self):
        return self._dirty


    def mark_clean(self):
        self._dirty = False


    def on_link_message(self, client, userdata, msg):
        topic = msg.topic
        if self._describe_suffix and topic.endswith(self._describe_suffix):
            # Describe message - update link if needed
            topic = topic[:-(len(self._describe_suffix)+1)]
            if topic in self._links:
                link = self._links[topic]
                if not link.datainfo:
                    try:
                        payload = json.loads(msg.payload)
                        if not isinstance(payload, list):
                            raise TypeError(f"Describe message should be SECoP formatted, ie enclosed inside a list. Got {payload.__class__.__name__}")
                        conf = payload[0]
                        if not isinstance(conf, dict):
                            raise TypeError(f"Describe message value should be a dict. Got {conf.__class__.__name__}")
                    except Exception as err:
                        # Silently ignore incorrect describe messages
                        return
                    try:
                        link.update(conf)
                    except Exception as err:
                        # Silently ignore errors when updating link
                        return
        if not topic in self._links:
            link_conf = {
                'value topic': topic,
                'readonly': True,
                'datasource id': self.id
            }
            link = MQTTLink(link_conf)
            self._links[topic] = link
            self._dirty = True


    def serialize(self):
        links = []
        for link in self._links.values():
            links.append(link.serialize())
        structure = {
            'links': links,
            'prefix': self._prefix,
            'datasource id': self.id,
            'type': 'mqtt',
            't': time.time(),
            'status': 'connected'  # An MQTT datasource can't be offline if the rest of the system is working so this value is hardcoded.
        }
        return structure


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--host',               help="MQTT host/IP", type=str, default="localhost")
    arg_parser.add_argument('--port',               help="MQTT port", type=int, default=1883)
    arg_parser.add_argument('--username', '--user', help="MQTT username", type=str, default="")
    arg_parser.add_argument('--password', '--pass', help="MQTT password", type=str, default="")
    arg_parser.add_argument('--prefix',             help="MQTT prefix", type=str, default="")
    args = arg_parser.parse_args()
    config = {
        'mqtt': {}
    }
    if args.host:
        config['mqtt']['host'] = args.host
    if args.port:
        config['mqtt']['port'] = args.port
    if args.username:
        config['mqtt']['username'] = args.username
    if args.password:
        config['mqtt']['password'] = args.password
    if args.prefix:
        config['mqtt']['prefix'] = args.prefix

    dsm = MQTTDatasourceManager(config)
    dsm.start()
