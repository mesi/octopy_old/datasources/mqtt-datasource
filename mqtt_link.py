from lib.links.base_link import BaseLink


class MQTTLink(BaseLink):
    def deserialize(self, conf):
        super().deserialize(conf)
        self.link_type = 'mqtt'
